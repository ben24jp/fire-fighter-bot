#include <Arduino.h>
#include "Pins.h" // Added all the pins are added here
#include "Detection.h" // The Sensor library
#include "Pump.h" // The water pump library
#include "Motion.h" // The bot motion library
#include "Servo.h" // The wiper servo lubrary

bool fire = 0;

// defined the pins for sensors 
Detection Sensor1 = Detection(S1);
Detection Sensor2 = Detection(S2);
Detection Sensor3 = Detection(S3);

// defined the pins for movement motors
Motion motion = Motion(MLt1, MLt2, MRt1, MRt2);

// define the pin for pump motor
Pump water = Pump(PM);

// devine the pin for sprinkler servo motor
Servo Sprinkler = Servo(SM);

void setup()
{
  // Initialized all pins used
  motion.initialize();
  water.initialize();
  Sensor1.initialize();
  Sensor2.initialize();
  Sensor3.initialize();
  Sprinkler.initialize();

  // Testing purpose only remove after testing
  Serial.begin(9600);
}

void loop() 
{
  if (Sensor1.sensorRead()==1 && Sensor2.sensorRead()==1 && Sensor3.sensorRead()==1) // No fire condition
  {
    // Don't move the BOT
    motion.stop();
  }
  else if (Sensor2.sensorRead()==0) // if fire is straight ahead
  {
    // Move the BOT forward
    motion.moveForward();
    fire = 1;
  }
  else if (Sensor1.sensorRead()==0) // if fire is to the left
  {
    // Turn the BOT to left
    motion.turnLeft();
  }
  else if (Sensor3.sensorRead()==0) // if fire is to right
  {
    // Turn the bot to right
    motion.turnRight();
  }
  delay(300); // Slowing down the BOT
  while (fire == 1)
  {
    water.spray();
    motion.stop();
    Sprinkler.sweep(180, 10);
  }
}