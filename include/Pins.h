#ifndef Pins_h
#define Pins_h

// The pins for Sensors are defined here
int S1 = A0;
int S2 = A1;
int S3 = A2;

// The pins for motion motors are declared here
int MLt1 = 7;
int MLt2 = 5;
int MRt1 = 8;
int MRt2 = 6;

// The pin for pump motor is dclared here
int PM = 3;

// The pin for the servo motor is decclared here
int SM = 11;

#endif