#include <Arduino.h> // Importing the Arduino library
#include "Pump.h" // Imported pump header file

Pump::Pump(int pumpPin)
{
  // passing pump pin
  _pumpPin = pumpPin;
}

void Pump::initialize()
{
  // Tnitializing the pump
  pinMode(_pumpPin, OUTPUT);
}

void Pump::spray()
{
  // Turning on the pump
  digitalWrite(_pumpPin, HIGH);
}


void Pump::dontSpray()
{
  // Turning of the pump
  digitalWrite(_pumpPin, LOW);
}