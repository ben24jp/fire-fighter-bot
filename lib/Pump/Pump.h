#ifndef Pump_h
#define Pump_h

class Pump
{
private:
  int _pumpPin;
public:
  Pump(int pumpPin);
  void initialize();
  void spray();
  void dontSpray();
};
#endif