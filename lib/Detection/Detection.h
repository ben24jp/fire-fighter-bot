#ifndef Detection_h
#define Detection_h

class Detection
{
private:
  int _sensorpin;
public:
  Detection(int sensorpin);
  void initialize();
  int sensorRead();
};

#endif