#include <Arduino.h> // Importing the Arduino library
#include "Detection.h" // Including the header for 

Detection::Detection(int sensorpin)
{
  _sensorpin = _sensorpin; // Passing pin to private variable
}

void Detection::initialize()
{
  pinMode(_sensorpin, INPUT); // Initializing the pin for sensor
}

int Detection::sensorRead()
{
  int tmp; // Teporary variable for storing data from sensor
  tmp = digitalRead(_sensorpin); // Reading the sensor pin
  return tmp; // Returning the readed values
}