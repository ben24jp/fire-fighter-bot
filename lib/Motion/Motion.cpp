#include "Arduino.h" // Importing the Arduino library
#include "Motion.h" // Importing motion library

// Variables to control the motor inputss
int ML1;
int ML2;
int MR1;
int MR2;

Motor Left = Motor(ML1, ML2); // Defining the left motor
Motor Right = Motor(MR1, MR2); // Definig the right motor

Motion::Motion(int L1, int L2, int R1, int R2)
{
  // Passing the variables
  _L1 = L1;
  _L2 = L2;
  _R1 = R1;
  _R2 = R2;
  ML1 = _L1;
  ML2 = _L2;
  MR1 = _R1;
  MR2 = _R2;
}

void Motion::initialize() // Initialized the output of motors
{
  pinMode(_L1, OUTPUT);
  pinMode(_L2, OUTPUT);
  pinMode(_R1, OUTPUT);
  pinMode(_R2, OUTPUT);
}

void Motion::moveForward() // Forward motion
{
  Left.forward();
  Right.forward();
}

void Motion::moveBackward() // Backward motion
{
  Left.backward();
  Right.backward();
}

void Motion::turnLeft() // Left turn motion
{
  Left.backward();
  Right.forward();
}

void Motion::turnRight() // Right turn motion
{
  Left.forward();
  Right.backward();
}

void Motion::stop() // Stop the motion
{
  Left.dontRun();
  Right.dontRun();
}