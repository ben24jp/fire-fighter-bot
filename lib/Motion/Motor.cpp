#include "Arduino.h" // Importing the Arduino library
#include "Motor.h" // Importing the the motor library

Motor::Motor(int t1, int t2)
{
  // Passing the variables and initializing the o=outputs
  pinMode(t1, OUTPUT);
  pinMode(t2, OUTPUT);
  _t1 = t1;
  _t2 = t2;
}

void Motor::forward() // Motor forward motion
{
  digitalWrite(_t1, LOW);
  digitalWrite(_t2, HIGH);
}

void Motor::backward() // Motor reverse motion
{
  digitalWrite(_t1, HIGH);
  digitalWrite(_t2, LOW); 
}

void Motor::dontRun() // Stop all the motor motion
{
  digitalWrite(_t1, LOW);
  digitalWrite(_t2, LOW);
}
