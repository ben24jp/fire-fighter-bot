#ifndef Motor_h
#define Motor_h

#include "Arduino.h"

class Motor
{
private:
  int _t1;
  int _t2;
public:
  Motor(int t1, int t2);
  void forward();
  void backward();
  void dontRun();
};

#endif