#ifndef Motion_h
#define Motion_h

#include "Arduino.h"
#include "Motor.h"
class Motion
{
private:
  int _L1;
  int _L2;
  int _R1;
  int _R2;
public:
  Motion(int L1, int L2, int R1, int R2);
  void initialize();
  void moveForward();
  void moveBackward();
  void turnLeft();
  void turnRight();
  void stop();
};

#endif