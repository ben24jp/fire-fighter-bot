#include <Arduino.h> // Importing the Arduino library
#include "Servo.h" // Importing servo header

Servo::Servo(int servoPin)
{
  // passing the pin number
  _servoPin = servoPin;
}

void Servo::initialize()
{
  // Initializing the pin
  pinMode(_servoPin, OUTPUT);
}

void Servo::sweep(int angle, int speed)  // Sweep portion starts here
{
  for (int i = 0; i <= angle; i += speed)
  {
    int pwm = (i*11) + 500;      // Convert angle to microseconds 
    digitalWrite(_servoPin, HIGH);
    delayMicroseconds(pwm);
    digitalWrite(_servoPin, LOW);
    delay(50);                   // Refresh cycle of servo
  }
  for (int i = angle; i >= 0; i -= speed)
  {
    int pwm = (i*11) + 500;      // Convert angle to microseconds
    digitalWrite(_servoPin, HIGH);
    delayMicroseconds(pwm);
    digitalWrite(_servoPin, LOW);
    delay(50);                   // Refresh cycle of servo
  }
  
}