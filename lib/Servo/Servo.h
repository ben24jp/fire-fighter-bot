#ifndef Servo_h
#define Servo_h

class Servo
{
private:
  int _servoPin;
public:
  Servo(int servoPin);
  void initialize();
  void sweep(int angle, int speed);
};

#endif