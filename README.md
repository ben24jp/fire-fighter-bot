<h1>Fire Fighter Bot</h1>


This is the design project that I have done in my 5th semster Engineering.
A simple fire fighting robot that detects fire and extinguishes it with water.
The code is writen in C++ and C using PlatformIO in Visual Studio Code.
A custom ATMega328P-PU based microcontroller board is used in this project.
lib folder contains the libraries that is been used, include folder contains
the header files that is been used and the src folder contains the combined code.


___
<h4>Electronic Components used</h4>

| **Componets** | **Quantity** |
| ------------- | ------------ |
| ATMega328P-PU | 1 |
| Geared DC Motor | 4 |
| Servo Motor | 1 |
| Flame Detector IR Sensor Module | 3 |
| IC LM7805 | 1 |
| IC LM7809 | 1 |
| IC L293D  | 1 |
| 22pF Capacitor (*Ceramic Disc type*) | 2 |
| 16MHz Crystal Oscillator | 1 |
| SL100 (*NPM Transistor*) | 1 |
| Boost Converter (*5V to 12V*) | 1 |
| Battery Pack (*4 Cells each with 4000mAh capacity at 3.7V*) | 1 |
| RMC Connector (*1x2*) |  |
| RMC Connector (*1x3*) |  |

___

<h4>Other Materials used</h4>

| **Mateials** | **Quantity** |
| ------------ | ------------ |
| Tyre (*with rubber bush*) | 4 |
___
<h7>*(more info to be added the project is under development)*</h7>